import paho.mqtt.client as mqtt
import time
import json
import logging

logging.basicConfig(filename='log.log', level=logging.INFO, filemode='w', format='%(name)s - %(levelname)s - %(message)s')

subscribed_topic = "MotionSensor"

def on_connect(client, userdata, flags, return_code):
    if return_code == 0:
        print("connected")
        client.subscribe(subscribed_topic)
    else:
        print("could not connect, return code:", return_code)

def appendToFile(message):
    logging.info(message)

def decode_json_to_log_string(json_text):
    loaded_object = json.loads(json_text)
    return f"{loaded_object['timestamp']} new event detected in {subscribed_topic}: movement in room {loaded_object['picture']}"

def on_message(client, userdata, message):
    decoded_string = str(message.payload.decode("utf-8"))
    print("Received message: " ,decoded_string)
    appendToFile(decode_json_to_log_string(decoded_string))


broker_hostname ="localhost"
port = 8883 

client = mqtt.Client("Client2")
# change with your user and password auth
client.username_pw_set(username="user1", password="password1")
client.on_connect=on_connect
client.on_message=on_message

client.connect(broker_hostname, port) 
client.loop_start()

try:
    time.sleep(10)
finally:
    client.loop_stop()