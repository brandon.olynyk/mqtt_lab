import paho.mqtt.client as mqtt 
import time
from datetime import datetime
import json 
broker_hostname = "localhost"
port = 8883 

def on_connect(client, userdata, flags, return_code):
    print("CONNACK received with code %s." % return_code)
    if return_code == 0:
        print("connected")
    else:
        print("could not connect, return code:", return_code)

client = mqtt.Client(client_id="Client1", userdata=None)
client.on_connect = on_connect

# change with your user and password auth
client.username_pw_set(username="user2", password="password2")

motion_var = True

def generate_message_json():
    return json.dumps({
        'motion_door': True,
        'timestamp': str(datetime.now()),
        'picture': f'http://somesite.com/api/image/{str(datetime.now())}'
    })


def on_motion_triggered(client, topic):
    message = generate_message_json() # should be set by timestamp bullshittery
    result = client.publish(topic=topic, payload=message)
    status = result[0]
    if status == 0:
        print("Message "+ message + " is published to topic " + topic)
    else:
        print("Failed to send message to topic " + topic)


def toggleMotionVar(client,topic):
    #toggle the motionvar. If it's true after
    #the toggle, send the message
    #print("Motion var toggled to " + str(motion_var))
    new_motion_var = not motion_var
    if not new_motion_var: 
        on_motion_triggered(client, topic)
    return new_motion_var
        




client.connect(broker_hostname, port, 60)
#client.loop_forever()
client.loop_start()


topic = "MotionSensor"
msg_count = 0

try:
    
    while msg_count < 100:
        time.sleep(1)
        msg_count += 1
        motion_var = toggleMotionVar(client, topic)
            
            
            
finally:
    client.loop_stop()
    
